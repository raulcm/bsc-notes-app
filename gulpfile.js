var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var webpack = require('webpack');
var express = require('express');
var karma = require('karma');
var reload = require('reload');
var http = require('http');
var browserSync = require('browser-sync').create();

var PORT = 9000;
var app;
var server;

gulp.task('express', function () {
    app = express();

    app.use(express.static(path.resolve('./')));
    app.set('port', PORT);

    server = http.createServer(app);
    gutil.log('Listening on port: ' + PORT);
    server.listen(app.get('port'))
});

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: 'localhost:9000'
    });
});

gulp.task('build', function (callback) {
    var config = require('./webpack.config.js');
    config.devtool = 'sourcemap';
    var devCompiler = webpack(config);
    devCompiler.run(function (err, stats) {

        if (err) {
            throw new gutil.PluginError('dev', err);
        }

        gutil.log('[dev]', stats.toString({
            colors: true
        }));
        callback();
    });
});

gulp.task('test', function (done) {
    new karma.Server({
        configFile: path.resolve(__dirname, 'karma.conf.js'),
        singleRun: true,
        watch: false
    }, done).start();
});

gulp.task('reload', function () {
    reload(server, app);
});

gulp.task('dev', ['build', 'express', 'browser-sync'], function () {
    gulp.watch(['./index.html', 'src/**/*.ts', 'src/**/*.html'],['build', 'reload'], function () {
        gulp.run('test');
    });
});