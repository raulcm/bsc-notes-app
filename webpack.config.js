var webpack = require('webpack');
var excludes = [
    /node_modules/
];

module.exports = {
    context: __dirname,
    devtool: 'sourcemap',
    entry: './src/main.ts',
    output: {
        path: __dirname + '/src/build',
        filename: 'notes.min.js'
    },
    debug: true,
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        // new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: true }),
    ],
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.ts']
    },
    externals: {
        angular: 'angular',
        jquery: 'jQuery'
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'webpack-typescript'
            },
            {
                test: /\.html$/,
                loader: 'html',
                excludes: /node_modules/
            },
            { test: require.resolve('angular'), loader: 'expose?angular' },
            { test: require.resolve('jquery'), loader: 'expose?jQuery' },
            { test: require.resolve('jquery'), loader: 'expose?$' },
        ]
    }
};