import { SUCCESS_MESSAGE, FAIL_MESSAGE } from '../constants/messageTypes';
import { ADD_MESSAGE } from '../constants/events';

let _notes: INotes;
let _notesService;
let _scope;

export class Details {
    private note: { id: string, title: string };
    private editForm: angular.IFormController;
    private deleteForm: angular.IFormController;
    private preventDelete: boolean = true;

    static $inject = ['$scope', 'Notes', 'NotesService', 'note'];

    constructor($scope: angular.IScope, notes: INotes, notesService: INotesService, note: INote) {
        notes.setCurrentNote(note);
        this.note = Object.create(note);
        _scope = $scope;
        _notesService = notesService;
        _notes = notes;
    }

    edit($event: angular.IAngularEvent) {
        if (this.editForm.$valid) {
            _notesService.update(this.note)
                .then(note => {
                    const currentNote = _notes.getCurrentNote();
                    const { title } = note;
                    currentNote.hidratate({
                        title
                    });
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: `Successfully note updated`,
                        messageType: SUCCESS_MESSAGE
                    });
                })
                .catch(error => {
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: 'There was an error updating the note',
                        messageType: FAIL_MESSAGE
                    });
                    throw "BOOM WHEN EDITING";
                }).finally(() => {
                    this.editForm.$setPristine();
                });
        }
        $event.preventDefault();
    }

    delete($event: angular.IAngularEvent) {
        this.deleteForm.$setDirty();
        if (!this.preventDelete) {
            _notesService.delete(this.note.id)
                .then(() => {
                    this.preventDelete = true;
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: `Successfully note deleted`,
                        messageType: SUCCESS_MESSAGE
                    });
                })
                .catch(error => {
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: 'There was an error deleting the error',
                        messageType: FAIL_MESSAGE
                    });
                    throw "THE NOTE COULD NOT BE DELETED";
                }).finally(() => {
                    this.deleteForm.$setPristine();
                });
        }
        $event.preventDefault();
    }
}