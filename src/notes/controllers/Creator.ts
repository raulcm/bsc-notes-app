import { ADD_MESSAGE } from '../constants/events';
import { SUCCESS_MESSAGE, FAIL_MESSAGE } from '../constants/messageTypes';
import { get } from 'lodash';

let _notesService: INotesService;
let _scope: angular.IScope;

export class Creator {
    private note: { title };
    private form: angular.IFormController;

    static $inject = ['$scope', 'NotesService'];

    constructor($scope: angular.IScope, notesService: INotesService) {
        _notesService = notesService;
        _scope = $scope;
        this.note = {
            title: ''
        };
    }

    submit($event: angular.IAngularEvent) {

        if (this.form.$valid) {
            _notesService.create(this.note)
                .then(({ data }) => {
                    this.note.title = '';
                    const title = get(data, 'title');
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: `Added note with title ${title}`,
                        messageType: SUCCESS_MESSAGE
                    });
                })
                .catch(error => {
                    _scope.$broadcast(ADD_MESSAGE, {
                        message: 'There was an error trying to create the note',
                        messageType: FAIL_MESSAGE
                    });
                    throw "BOOM WHEN CREATING";
                });
        }
        $event.preventDefault();
    }
}
