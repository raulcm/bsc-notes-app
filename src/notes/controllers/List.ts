export class List {
    static $inject: Array<string> = ['Notes', 'notes'];

    constructor(Notes: INotes, notes: Array<INote>) {
        Notes.setNotes(notes);
    }
}