interface INote {
    getId(): string;
    getTitle(): string,
    hidratate(note: { title: string }): void;
}

interface INotes {
    setNotes(notes: INote[]);
    get(index: number): INote;
    setCurrentNote(note: INote);
    getCurrentNote(): INote;
    clearCurrentNote();
    size(): number;
    find(id: string): INote;
    findIdx(id: string): number;
}

interface INotesPaginator {
    isInRange(page: number, limit: number): boolean;
    setItemsPerPage(itemsAmount: number);
    get(page: number, notesService: INotes): INote[];
    getItemsPerPage(): number;
    setItemsPerPage(amount: number): void;
    getCurrentPage(): number;
}

interface INotesService {
    fetchAll(): angular.IHttpPromise<INote[]>;
    fetch(id: string): angular.IHttpPromise<INote>;
    create(note: { title: string}): angular.IHttpPromise<INote>;
    update(note: { id: string, title: string }): angular.IHttpPromise<INote>;
    delete(id: string): angular.IHttpPromise<any>;
}