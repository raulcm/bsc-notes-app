import { range, identity, find } from 'lodash';

let _http: angular.IHttpService;
let _url: string;

export class Note implements INote {
    private id: string;
    private title: string;

    constructor({ id, title }) {
        this.id = `${id}`;
        this.title = title;
    }

    getId() {
        return this.id;
    }

    getTitle() {
        return this.title;
    }

    hidratate(note: { title: string }) {
        this.title = note.title
    }

    toString() {
        return JSON.stringify({ id: this.id, title: this.title });
    }
}

export class Notes implements INotes {
    private notes: INote[] = [];
    private currentNote: INote;

    setNotes(notes: INote[]) {
        this.notes = notes;
    }

    get(idx: number) {
        if (idx >= this.notes.length) {
            return;
        }

        return this.notes[idx];
    }

    setCurrentNote(note: INote) {
        this.currentNote = note;
    }

    getCurrentNote() {
        return this.currentNote;
    }

    clearCurrentNote() {
        this.setCurrentNote(null);
    }

    size() {
        return this.notes.length;
    }

    findIdx(id: string) {
        return find(range(0, this.size()), (idx) => {
            const note = this.get(idx);

            return !note ? !!note : note.getId() === id;
        });
    }

    find(id: string): INote {
        const noteIndex = this.findIdx(id);

        return this.get(noteIndex);
    }
}

export class NotesService implements INotesService {

    constructor($http: angular.IHttpService, url) {
        _http = $http;
        _url = url;
    }

    fetchAll(): angular.IHttpPromise<INote[]> {
        return _http.get(_url).then((response: { data: Array<any>}) => {
            return response.data.map((note: any) => {
                return new Note({
                    id: note.id,
                    title: note.title
                });
            });
        });
    }

    fetch(id) {
        return _http.get(`${_url}/${id}`).then((response: { data: { id: string, title } }) => new Note({
            id: response.data.id,
            title: response.data.title
        }));
    }

    create(note: { title }) {
        return _http.post(_url, note).then(response => response.data);
    }

    update(note: { id: string, title: string}) {
        return _http.put(`${_url}/${note.id}`, note).then(response => response.data);
    }

    delete(id: string) {
        return _http.delete(`${_url}/${id}`)
    }
}

export class NotesPaginator {
    private currentPage: number = 0;
    private offset: number = 0;
    private itemsPerPage = 0;

    get(page: number, notesService: INotes): INote[] {
        if (!this.isInRange(page, notesService.size())) {
            throw Error(`Page ${page} is not in range`);
        }

        this.offset = this.itemsPerPage * page;

        this.currentPage = page;

        const indexes = range((page - 1) * this.offset, this.offset + this.itemsPerPage);
        return indexes.map(idx => notesService.get(idx))
            .filter(identity);
    }

    isInRange(page, limit) {
        return page <= Math.ceil(limit / this.itemsPerPage);
    }

    setItemsPerPage(amount: number) {
        this.itemsPerPage = amount;
    }

    getItemsPerPage(): number {
        return this.itemsPerPage;
    }

    getCurrentPage() {
        return this.currentPage || 1;
    }
}
