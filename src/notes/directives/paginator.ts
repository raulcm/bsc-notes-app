import { range } from 'lodash';
import { FIRST_PAGE, PREVIOUS_PAGE, NEXT_PAGE, LAST_PAGE } from '../constants/events';

const DEFAULT_NOTES_PER_PAGE = 5;

let _notes: INotes;
let _notesPaginator: INotesPaginator;

class NotesGridPaginator implements angular.IDirective {
    restrict: string = 'A';
    template: string = require('./templates/paginator.html');
    scope: any = {
    };

    link(scope, element, attrs): void {

        if (!_notesPaginator.getItemsPerPage()) {
            _notesPaginator.setItemsPerPage(DEFAULT_NOTES_PER_PAGE);
        }

        scope.currentPage = _notesPaginator.getCurrentPage();

        scope.pages = range(scope.currentPage || 1, _notes.size(),
            _notesPaginator.getItemsPerPage());

        scope.first = ($event: angular.IAngularEvent) => {
            if (!scope.isFirst()) {
                scope.$emit(FIRST_PAGE, 1);
            }
            $event.preventDefault();
        };

        scope.previous = ($event: angular.IAngularEvent) => {
            if (!scope.isFirst()) {
                scope.$emit(PREVIOUS_PAGE, scope.pages[scope.currentPage - 1]);
            }
            $event.preventDefault();
        };

        scope.next = ($event: angular.IAngularEvent) => {
            if (!scope.isLast()) {
                scope.$emit(NEXT_PAGE, scope.pages[scope.currentPage + 1]);
            }
            $event.preventDefault();
        };

        scope.last = ($event: angular.IAngularEvent) => {
            if (!scope.isLast()) {
                scope.$emit(LAST_PAGE, scope.pages[scope.pages.length - 1]);
            }
            $event.preventDefault();
        };

        scope.isFirst = () => scope.currentPage < 2;
        scope.isLast = () => scope.currentPage >= (scope.pages.length - 1);
    }
}

export const notesGridPaginatorFactory: angular.IDirectiveFactory =
    (notes: INotes, notesPaginator) => {
        _notes = notes;
        _notesPaginator = notesPaginator;
        return new NotesGridPaginator();
    };

notesGridPaginatorFactory.$inject = ['Notes', 'NotesPaginator'];
