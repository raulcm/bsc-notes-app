import { FIRST_PAGE, PREVIOUS_PAGE, NEXT_PAGE, LAST_PAGE } from '../constants/events';
import { partial } from 'lodash';

let _notes: INotes;
let _notesPaginator: INotesPaginator;

const loadPage = (scope: { items: INote[] }, page: number) => {
    scope.items = _notesPaginator.get(page, _notes);
}

class NotesGrid implements angular.IDirective {
    restrict: string = 'A';
    template: string = require('./templates/grid.html');
    scope: any = {};

    link(scope, element, attrs): void {
        const loadPageHandler = partial(loadPage, scope);
        scope.items = _notesPaginator.get(1, _notes);

        scope.$on(FIRST_PAGE, loadPageHandler);

        scope.$on(PREVIOUS_PAGE, loadPageHandler);

        scope.$on(NEXT_PAGE, loadPageHandler);

        scope.$on(LAST_PAGE, loadPageHandler);
    }
}

export const notesGridFactory: angular.IDirectiveFactory =
    (notes: INotes, notesPaginator: INotesPaginator) => {
        _notes = notes;
        _notesPaginator = notesPaginator;
        return new NotesGrid();
    };

notesGridFactory.$inject = ['Notes', 'NotesPaginator'];