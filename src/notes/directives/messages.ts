import { SUCCESS_MESSAGE, FAIL_MESSAGE } from '../constants/messageTypes';
import { ADD_MESSAGE } from '../constants/events';
import { uniqueId, find, partial } from 'lodash';

const DEFAULT_MESSAGE = '@@noteTitle@@';
const replaceMessageType = (type: string, template: string) => {
    return template.replace(
        /@@messageType@@/,
        type.toLowerCase().replace(/_/, '-'));
};
const getDefaultMessage = ({ title }) => {
    return DEFAULT_MESSAGE.replace(/@@noteTitle@@/, title);
};

let _timeout: angular.ITimeoutService;
let _state: angular.ui.IStateService;

class NotesMessage implements angular.IDirective {
    restrict: string = 'A';
    scope: any = {};

    link(scope, element, attrs): void {
        const messageIds = [];
        const removeMessage = (id) => {
            const idx = find(messageIds, message => message === id);
            messageIds.splice(idx, 1);
        };

        scope.$on(ADD_MESSAGE, function ($event, { note, messageType, message }) {
            const id = uniqueId(Date.now().toString(16));
            const goToList = partial((messageType) => {
                if (messageType === SUCCESS_MESSAGE) {
                    _state.transitionTo('notes.list', {}, { reload: true });
                }
            }, messageType);

            let template: string = require('./templates/messages.html');

            if (messageType === SUCCESS_MESSAGE) {
                template = replaceMessageType(SUCCESS_MESSAGE, template);
                template = template.replace(/@@alertType@@/, 'success');
            }

            if (messageType === FAIL_MESSAGE) {
                template = replaceMessageType(FAIL_MESSAGE, template);
                template = template.replace(/@@alertType@@/, 'danger');
            }

            template = template.replace(
                /@@message@@/,
                message || getDefaultMessage(note));

            template = template.replace(/@@id@@/g, id);

            element.append(template);

            messageIds.push(id);

            _timeout(() => {
                element.find(`#${id}`).remove();
                removeMessage(id);
                goToList();
            }, 3000);
        });

        element.on('click', function ($event: JQueryEventObject) {
            const $el = $($event.target);
            $el.parents('.alert').remove();
            removeMessage($el.prop('id'));
        });

        scope.$on('$destroy', function () {
            element.off('click');
        });
    }
}

export const notesMessageFactory: angular.IDirectiveFactory =
    ($timeout: angular.ITimeoutService, $state: angular.ui.IStateService) => {
        _timeout = $timeout;
        _state = $state;
        return new NotesMessage();
    };

notesMessageFactory.$inject = ['$timeout', '$state'];
