import { Note } from './NotesService';
import { range, find } from 'lodash';

const notesLoader = (NotesService: INotesService) => {
    return NotesService.fetchAll();
}

notesLoader.$inject = ['NotesService'];

const loadNote = function ($q, $stateParams, Notes: INotes, NotesService: INotesService, url) {
    const id = $stateParams.noteId;
    const isNoteServiceLoaded = Notes.size() > 0;

    if (!isNoteServiceLoaded) {
        return NotesService.fetch(id);
    }

    return $q.when(Notes.find(id)).then(note => {
        if (note instanceof Note) {
            return note;
        }

        new Note(note);
    });
}

loadNote.$inject = ['$q', '$stateParams', 'Notes', 'NotesService', 'url'];

function _config($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/notes/list');

    $stateProvider
        .state('notes', {
            url: '/notes',
            templateProvider: () => require('./index.html')
        })
        .state('notes.list', {
            url: '/list',
            templateProvider: () => require('./controllers/templates/list.html'),
            controllerAs: 'list',
            controller: 'NotesListController',
            resolve: {
                notes: notesLoader
            }
        })
        .state('notes.create', {
            url: '/new',
            templateProvider: () => require('./controllers/templates/create.html'),
            controllerAs: 'creator',
            controller: 'NoteCreatorController'
        })
        .state('notes.details', {
            url: '/view/:noteId',
            templateProvider: () => require('./controllers/templates/details.html'),
            controllerAs: 'details',
            controller: 'NoteDetailsController',
            resolve: {
                note: loadNote
            }
        })
        .state('notes.details.edit', {
            url: '/edit',
            templateProvider: () => require('./controllers/templates/edit.html')
        })
        .state('notes.details.delete', {
            url: '/delete',
            templateProvider: () => require('./controllers/templates/delete.html')
        });

    $httpProvider.defaults.headers = {
        common: {
            'X-Requested-With': 'XDomainRequest',
            'Accept': 'application/json',
        }
    };
};

_config.$inject = [
    '$stateProvider',
    '$urlRouterProvider',
    '$httpProvider'
];

export const config = _config;