import { config } from './config';
import { List } from './controllers/List';
import { Creator } from './controllers/Creator';
import { Details } from './controllers/Details';
import { notesGridFactory } from './directives/grid';
import { notesGridPaginatorFactory } from './directives/paginator';
import { notesMessageFactory } from './directives/messages';
import { Notes, NotesPaginator, NotesService } from './NotesService';

export const name = 'notes';

function run() {

}

run.$inject = [];

angular.module(name, [])
    .service('NotesService', NotesService)
    .service('NotesPaginator', NotesPaginator)
    .service('Notes', Notes)
    .directive('notesGrid', notesGridFactory)
    .directive('notesGridPaginator', notesGridPaginatorFactory)
    .directive('notesMessages', notesMessageFactory)
    .controller('NotesListController', List)
    .controller('NoteCreatorController', Creator)
    .controller('NoteDetailsController', Details)
    .config(config)
    .run(run);

