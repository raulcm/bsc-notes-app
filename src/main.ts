import * as notesModule from './notes/main';
import { UrlProvider } from './UrlProvider';

const config = (UrlProvider) => {
    UrlProvider.set('http://private-anon-acc49208f-note10.apiary-mock.com/notes');
}

config.$inject = ['urlProvider'];

angular.module('bsc', [
    'ui.router',
    'ngMessages',
    notesModule.name
]).provider('url', UrlProvider)
    .config(config);
