let _location: angular.ILocationProvider;

export class UrlProvider {
    private url: string;

    set(url: string) {
        this.url = url;
    }

    $get() {
        return this.url;
    }
}