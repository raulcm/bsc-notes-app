# BSC Notes test

Installation
====

- Run `&> npm install` to install dependencies
- `&> gulp dev` starts the development environment
- `&> gulp express` starts the server at `http://localhost:9000`
- `&> gulp test` runs the unit-test
